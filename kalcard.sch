EESchema Schematic File Version 4
LIBS:kalcard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "kalstock board"
Date "2019-06-08"
Rev "1.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x05_Female J3_max_spi1
U 1 1 5C9F8499
P 1650 3470
F 0 "J3_max_spi1" V 1530 3710 50  0000 L CNN
F 1 "Conn_01x05_Female" V 1690 3150 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 1650 3470 50  0001 C CNN
F 3 "~" H 1650 3470 50  0001 C CNN
	1    1650 3470
	0    1    1    0   
$EndComp
$Comp
L Sensor_Temperature:DS18B20 U1
U 1 1 5C9F86A5
P 3800 3000
F 0 "U1" H 3650 3250 50  0000 C CNN
F 1 "DS18B20" H 4040 2736 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 2800 2750 50  0001 C CNN
F 3 "http://datasheets.maximintegrated.com/en/ds/DS18B20.pdf" H 3650 3250 50  0001 C CNN
	1    3800 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 3150 750  3150
Wire Wire Line
	750  3150 750  3690
Wire Wire Line
	750  3690 3150 3690
Wire Wire Line
	3150 3690 3150 2450
Connection ~ 750  3150
$Comp
L Device:R_Small_US R1
U 1 1 5C9F963F
P 4150 2700
F 0 "R1" H 4200 2700 50  0000 L CNN
F 1 "4.7k" H 3950 2700 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 4150 2700 50  0001 C CNN
F 3 "~" H 4150 2700 50  0001 C CNN
	1    4150 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3000 4100 3000
Wire Wire Line
	4150 2450 4150 2600
Wire Wire Line
	4150 2800 4150 3000
Wire Wire Line
	1450 3150 1450 3270
Wire Wire Line
	950  3050 1550 3050
Wire Wire Line
	950  3050 950  3930
Wire Wire Line
	3800 3930 3800 3300
Text Notes 1700 3600 0    50   ~ 0
CS
Text Notes 1850 3600 0    50   ~ 0
SCK
Text Notes 1600 3750 0    50   ~ 0
DIN\nMOSI
Text Notes 1450 3600 0    50   ~ 0
GND
Text Notes 1250 3650 0    50   ~ 0
Vcc\n+5V
$Comp
L MCU_Module:Arduino_Nano_v3.x A1
U 1 1 5CFBDF86
P 2175 1595
F 0 "A1" H 2175 509 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 2175 418 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 2325 645 50  0001 L CNN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 2175 595 50  0001 C CNN
	1    2175 1595
	1    0    0    -1  
$EndComp
Wire Wire Line
	1675 2095 1545 2095
Wire Wire Line
	1545 2095 1545 2920
Wire Wire Line
	1545 2920 1650 2920
Wire Wire Line
	1650 2920 1650 3270
Wire Wire Line
	1675 1995 1605 1995
Wire Wire Line
	1605 1995 1605 2865
Wire Wire Line
	1605 2865 1750 2865
Wire Wire Line
	1750 2865 1750 3270
Wire Wire Line
	1675 2295 1635 2295
Wire Wire Line
	1635 2295 1635 2810
Wire Wire Line
	1635 2810 1850 2810
Wire Wire Line
	1850 2810 1850 3270
Wire Wire Line
	2375 595  2375 530 
Wire Wire Line
	2375 530  750  530 
Wire Wire Line
	750  530  750  3150
Wire Wire Line
	2275 2595 2275 2975
Wire Wire Line
	2275 2975 1550 2980
Wire Wire Line
	1550 2980 1550 3050
Connection ~ 1550 3050
Wire Wire Line
	1550 3050 1550 3270
NoConn ~ 2075 595 
NoConn ~ 2275 595 
NoConn ~ 2675 995 
NoConn ~ 2675 1095
NoConn ~ 2675 1395
NoConn ~ 2675 1595
NoConn ~ 2675 1695
NoConn ~ 2675 1795
NoConn ~ 2675 1895
NoConn ~ 2675 1995
NoConn ~ 2675 2095
NoConn ~ 2675 2195
NoConn ~ 2675 2295
NoConn ~ 1675 2195
NoConn ~ 1675 1795
NoConn ~ 1675 1695
NoConn ~ 1675 1595
NoConn ~ 1675 1495
NoConn ~ 1675 1395
NoConn ~ 1675 1295
NoConn ~ 1675 1195
NoConn ~ 1675 1095
NoConn ~ 1675 995 
Wire Wire Line
	1675 1895 1470 1895
Wire Wire Line
	1470 1895 1470 545 
Wire Wire Line
	1470 545  4405 545 
Wire Wire Line
	4405 545  4405 3000
Wire Wire Line
	4405 3000 4150 3000
Connection ~ 4150 3000
$Comp
L power:GND #PWR0102
U 1 1 5CFDAB0B
P 3320 4010
F 0 "#PWR0102" H 3320 3760 50  0001 C CNN
F 1 "GND" H 3325 3837 50  0000 C CNN
F 2 "" H 3320 4010 50  0001 C CNN
F 3 "" H 3320 4010 50  0001 C CNN
	1    3320 4010
	1    0    0    -1  
$EndComp
Wire Wire Line
	3320 4010 3320 3930
Wire Wire Line
	950  3930 3320 3930
Connection ~ 3320 3930
Wire Wire Line
	3320 3930 3800 3930
Wire Wire Line
	3150 2450 3800 2450
Wire Wire Line
	3800 2450 4150 2450
$Comp
L power:GND #PWR0104
U 1 1 5CFE11F7
P 1600 4565
F 0 "#PWR0104" H 1600 4315 50  0001 C CNN
F 1 "GND" H 1605 4392 50  0000 C CNN
F 2 "" H 1600 4565 50  0001 C CNN
F 3 "" H 1600 4565 50  0001 C CNN
	1    1600 4565
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5CFE1239
P 1245 4560
F 0 "#FLG0101" H 1245 4635 50  0001 C CNN
F 1 "PWR_FLAG" H 1245 4733 50  0000 C CNN
F 2 "" H 1245 4560 50  0001 C CNN
F 3 "~" H 1245 4560 50  0001 C CNN
	1    1245 4560
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5CFE125B
P 1600 4460
F 0 "#FLG0102" H 1600 4535 50  0001 C CNN
F 1 "PWR_FLAG" H 1600 4634 50  0000 C CNN
F 2 "" H 1600 4460 50  0001 C CNN
F 3 "~" H 1600 4460 50  0001 C CNN
	1    1600 4460
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 4460 1600 4565
Wire Wire Line
	1245 4455 1245 4560
Wire Wire Line
	3800 2700 3800 2450
Connection ~ 3800 2450
$Comp
L power:VCC #PWR0101
U 1 1 5CFE3392
P 1245 4455
F 0 "#PWR0101" H 1245 4305 50  0001 C CNN
F 1 "VCC" H 1262 4628 50  0000 C CNN
F 2 "" H 1245 4455 50  0001 C CNN
F 3 "" H 1245 4455 50  0001 C CNN
	1    1245 4455
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5CFE3897
P 4770 715
F 0 "H1" H 4870 761 50  0000 L CNN
F 1 "MountingHole" H 4870 670 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm" H 4770 715 50  0001 C CNN
F 3 "~" H 4770 715 50  0001 C CNN
	1    4770 715 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5CFE3939
P 4780 1005
F 0 "H2" H 4880 1051 50  0000 L CNN
F 1 "MountingHole" H 4880 960 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm" H 4780 1005 50  0001 C CNN
F 3 "~" H 4780 1005 50  0001 C CNN
	1    4780 1005
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5CFE39DF
P 4795 1290
F 0 "H3" H 4895 1336 50  0000 L CNN
F 1 "MountingHole" H 4895 1245 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm" H 4795 1290 50  0001 C CNN
F 3 "~" H 4795 1290 50  0001 C CNN
	1    4795 1290
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5CFE3A2F
P 4795 1595
F 0 "H4" H 4895 1641 50  0000 L CNN
F 1 "MountingHole" H 4895 1550 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm" H 4795 1595 50  0001 C CNN
F 3 "~" H 4795 1595 50  0001 C CNN
	1    4795 1595
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H5
U 1 1 5CFE3AA1
P 4800 1925
F 0 "H5" H 4900 1971 50  0000 L CNN
F 1 "MountingHole" H 4900 1880 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm" H 4800 1925 50  0001 C CNN
F 3 "~" H 4800 1925 50  0001 C CNN
	1    4800 1925
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CFE5145
P 1990 2680
F 0 "#PWR?" H 1990 2430 50  0001 C CNN
F 1 "GND" H 1995 2507 50  0000 C CNN
F 2 "" H 1990 2680 50  0001 C CNN
F 3 "" H 1990 2680 50  0001 C CNN
	1    1990 2680
	1    0    0    -1  
$EndComp
Wire Wire Line
	2175 2595 1990 2595
Wire Wire Line
	1990 2595 1990 2680
$EndSCHEMATC
